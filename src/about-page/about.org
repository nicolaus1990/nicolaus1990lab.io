#+title: About this blog and me.
#+date: <2018-06-13 00:00>
#+OPTIONS: toc:nil
# #+OPTIONS: toc:2

* HTML HEADER (CSS, and else)                                      :noexport:
# CSS Styles and icons
#+HTML_HEAD_EXTRA: <link href= "../resources/style.css" rel="stylesheet" type="text/css" />
#+HTML_HEAD_EXTRA: <link rel="icon" href="../resources/favicon.ico">
#+HTML_HEAD_EXTRA: <link rel="apple-touch-icon-precomposed" href="../resources/apple-touch-icon.png">
#+HTML_HEAD_EXTRA: <link rel="apple-touch-icon-precomposed" href="../resources/favicon-152.png">
#+HTML_HEAD_EXTRA: <link rel="msapplication-TitleImage" href="../resources/favicon-144.png">
#+HTML_HEAD_EXTRA: <link rel="msapplication-TitleColor" href="#0141ff">



* Hello web wanderers!

#+BEGIN_EXPORT html
<span >
 <img src="./profilePic.png" id="profilePic" alt="My profile picture" height="42" width="42">
</span>
#+END_EXPORT

This is my blog. It's still kind of empty. ...

Click [[file:./myCV.pdf][here]] for my CV.

#+BEGIN_EXPORT html
<center>
<a rel="license" href="https://creativecommons.org/licenses/by-sa/3.0/">
<img alt="Creative Commons License" style="border-width:0"
     src="https://i.creativecommons.org/l/by-sa/3.0/88x31.png" />
</a>
<br />
<span xmlns:dct="https://purl.org/dc/terms/" href="https://purl.org/dc/dcmitype/Text"
            property="dct:title" rel="dct:type">
	    Blog contents
</span>
by 
<a xmlns:cc="https://creativecommons.org/ns#"
         href="https://nicolaus1990.gitlab.io"
	 property="cc:attributionName" rel="cc:attributionURL">
N.M.</a> is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by-sa/3.0/">
Creative Commons Attribution-ShareAlike 3.0 Unported License
</a>.

<br />
<p>
This blog was created with emacs, org-mode and several functions from
<a href="https://github.com/bastibe/org-static-blog">org-static-blog</a>. 
My blog config is <a href="https://gitlab.com/nicolaus1990/nicolaus1990.gitlab.io">here</a>.
<br />
</p>
</center>
#+END_EXPORT
