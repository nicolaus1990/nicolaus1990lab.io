# Makefile to create blog

ELISP_SRC=./src_elisp_blog_maker/
MAIN_FILE=$(ELISP_SRC)make-blog.el
TARGET_FOLDER=./target

EMACS_CMD=emacs --no-init-file --no-desktop

# Elisp dependency
DEPS_1=~/.emacs.d/utilitiesElisp/n-elisp-tools-general.el
DEPS_2=~/.emacs.d/utilitiesElisp/n-elisp-tools-org-mode.el
DEPS_3=~/.emacs.d/utilitiesElisp/n-org-publish-helper-fns/n-org-publish-helper-fns.el
# DEPS_4=~/.emacs.d/htmlize.el

clean:
	rm -rf $(TARGET_FOLDER)

get-deps:
	cp $(DEPS_1) $(ELISP_SRC)
	cp $(DEPS_2) $(ELISP_SRC)
	cp $(DEPS_3) $(ELISP_SRC)

blog-with-drafts-force: clean 
	$(EMACS_CMD) --script $(MAIN_FILE) --eval="(n/personal-blog t t)"
blog-with-drafts:
	$(EMACS_CMD) --script $(MAIN_FILE) --eval="(n/personal-blog nil t)"


blog-final-force: clean
	$(EMACS_CMD) --script $(MAIN_FILE) --eval="(n/personal-blog t)"
blog-final: 
	$(EMACS_CMD) --script $(MAIN_FILE) --funcall=n/personal-blog
