;; [[file:../config.org::*Requirements][Requirements:1]]
(require 'n-elisp-tools-general) ;; For: n-foldl, n/match-a-line-in-file, n/get-files-sorted
;; Requirements:1 ends here

;; [[file:../config.org::*Creating unique custom_id properties][Creating unique custom_id properties:1]]
(defun n-my-generate-sanitized-alnum-dash-string(str)
"Returns a string which contains only a-zA-Z0-9 with single dashes
 replacing all other characters in-between them.

 Some parts were copied and adapted from org-hugo-slug
 from https://github.com/kaushalmodi/ox-hugo (GPLv3)."
(let* (;; Remove "<FOO>..</FOO>" HTML tags if present.
       (str (replace-regexp-in-string "<\\(?1:[a-z]+\\)[^>]*>.*</\\1>" "" str))
       ;; Remove URLs if present in the string.  The ")" in the
       ;; below regexp is the closing parenthesis of a Markdown
       ;; link: [Desc](Link).
       (str (replace-regexp-in-string (concat "\\](" ffap-url-regexp "[^)]+)") "]" str))
       ;; Replace "&" with " and ", "." with " dot ", "+" with
       ;; " plus ".
       (str (replace-regexp-in-string
             "&" " and "
             (replace-regexp-in-string
              "\\." " dot "
              (replace-regexp-in-string
               "\\+" " plus " str))))
       ;; Replace German Umlauts with 7-bit ASCII.
       (str (replace-regexp-in-string "[Ä]" "Ae" str t))
       (str (replace-regexp-in-string "[Ü]" "Ue" str t))
       (str (replace-regexp-in-string "[Ö]" "Oe" str t))
       (str (replace-regexp-in-string "[ä]" "ae" str t))
       (str (replace-regexp-in-string "[ü]" "ue" str t))
       (str (replace-regexp-in-string "[ö]" "oe" str t))
       (str (replace-regexp-in-string "[ß]" "ss" str t))
       ;; Replace all characters except alphabets, numbers and
       ;; parentheses with spaces.
       (str (replace-regexp-in-string "[^[:alnum:]()]" " " str))
       ;; On emacs 24.5, multibyte punctuation characters like "："
       ;; are considered as alphanumeric characters! Below evals to
       ;; non-nil on emacs 24.5:
       ;;   (string-match-p "[[:alnum:]]+" "：")
       ;; So replace them with space manually..
       (str (if (version< emacs-version "25.0")
                (let ((multibyte-punctuations-str "：")) ;String of multibyte punctuation chars
                  (replace-regexp-in-string (format "[%s]" multibyte-punctuations-str) " " str))
              str))
       ;; Remove leading and trailing whitespace.
       (str (replace-regexp-in-string "\\(^[[:space:]]*\\|[[:space:]]*$\\)" "" str))
       ;; Replace 2 or more spaces with a single space.
       (str (replace-regexp-in-string "[[:space:]]\\{2,\\}" " " str))
       ;; Replace parentheses with double-hyphens.
       (str (replace-regexp-in-string "\\s-*([[:space:]]*\\([^)]+?\\)[[:space:]]*)\\s-*" " -\\1- " str))
       ;; Remove any remaining parentheses character.
       (str (replace-regexp-in-string "[()]" "" str))
       ;; Replace spaces with hyphens.
       (str (replace-regexp-in-string " " "-" str))
       ;; Remove leading and trailing hyphens.
       (str (replace-regexp-in-string "\\(^[-]*\\|[-]*$\\)" "" str)))
  str)
)

(defun n-my-custom-id-get-or-generate()
"Returns the ID property if set or generates and returns a new one if not set.
 The generated ID is stripped off potential progress indicator cookies and
 sanitized to get a slug. Furthermore, it is prepended with an ISO date-stamp
 if none was found before."
    (interactive)
        (when (not (org-id-get))
            (progn
    	       (let* (
	              ;; retrieve heading string
    	              (my-heading-text (nth 4 (org-heading-components)))
		      ;; remove progress indicators like "[2/7]" or "[25%]"
		      (my-heading-text (replace-regexp-in-string "[[][0-9%/]+[]] " "" my-heading-text))
		      ;; get slug from heading text
                      (new-id (n-my-generate-sanitized-alnum-dash-string my-heading-text))
    	             )
                   (when (not (string-match "[12][0-9][0-9][0-9]-[01][0-9]-[0123][0-9]-.+" new-id))
                           ;; only if no ISO date-stamp is found at the beginning of the new id:
        	           (setq new-id (concat (format-time-string "%Y-%m-%d-") new-id)))
                   (org-set-property "CUSTOM_ID" new-id)
                   )
                 )
        )
	(kill-new (org-id-get));; put ID in kill-ring
	(org-id-get);; retrieve the current ID in any case as return value
)

;;(bind-key (kbd "I") #'my-id-get-or-generate my-map) ;; -> my binding
;; Creating unique custom_id properties:1 ends here

;; [[file:../config.org::*Easy entering values for emacs properties][Easy entering values for emacs properties:1]]
(defun n-org-read-entry-property-name ()
  "Read a property name from the current entry."
  (let ((completion-ignore-case t)
        (default-prop (or (and (org-at-property-p)
                               (org-match-string-no-properties 2))
                          org-last-set-property)))
    (org-completing-read
     (format "Property [%s]: " (if default-prop default-prop ""))
     (org-entry-properties nil nil)
     nil nil nil nil default-prop)))

(defun n-my-org-region-to-property (&optional property)
  "Copies the region as value to an Org-mode property"
  (interactive)
  ;; if no region is defined, do nothing
  (if (use-region-p)
      ;; if a region string is found, ask for a property and set property to
      ;; the string in the region
      (let ((val (replace-regexp-in-string
                  "\\`[ \t\n]*" ""
                  (replace-regexp-in-string "[ \t\n]*\\'" ""
                                            (substring (buffer-string)
                                                       (- (region-beginning) 1)
                                                       (region-end))))
                 )
            ;; if none was stated by user, read property from user
            (prop (or property
                      (n-org-read-entry-property-name))))
        ;; set property
        (org-set-property prop val))))
;; Easy entering values for emacs properties:1 ends here

;; [[file:../config.org::*Creating org buffer with links to all files in folders][Creating org buffer with links to all files in folders:1]]
;;----------------------------------------------------
;;
;;  _______                  __           __                    ___ __ __        
;; |       |.----.-----.    |__|.-----.--|  |.-----.--.--.    .'  _|__|  |.-----.
;; |   -   ||   _|  _  |    |  ||     |  _  ||  -__|_   _|    |   _|  |  ||  -__|
;; |_______||__| |___  |    |__||__|__|_____||_____|__.__|    |__| |__|__||_____|
;;               |_____|                                                         
;;
;;----------------------------------------------------



(defun n/folder-to-org-inserts (dir &optional show-hidden)
  "Inserts org links (and each folder is going to be a heading).

Note: Will not insert links that are in hidden folders or folders with prefix 'ignore' or 'zz'."
  (let ((files (n/get-files-sorted dir))
	(past-folders))
    (dolist (f files)
      ;; If file is not inside hidden folder
      (when (or show-hidden (not (or (string-match "/\\." f)
				     (string-match "/ignore" f)
				     (string-match "/\\#" f)
				     (string-match "/zzz" f)
				     ;;Files at root-level with prefixes
				     (string-match "^\\." f)
				     (string-match "^\\#" f)
				     (string-match "^zz" f)
				     )))
	(progn
	  ;; Add Headings (each header a folder) only if f is inside a folder.
	  (when (string-match "/" f);;if file is in at least one folder
	    (n-foldl '(lambda (xs next-str)
			(let ((folderLevel (car xs))
			      (folderName (cdr xs)))
			  (when (not (member folderName past-folders))
			    (insert (concat (make-string folderLevel ?*)
					    " " folderName "\n"))
			    (setq past-folders (cons folderName past-folders)))
			  (cons (+ 1 folderLevel) next-str)))
		     ;; List of folders and subfolders
		     (append (split-string 
			      ;;Remove file and last slash
			      (directory-file-name
			       (file-name-directory f))
			      "/")
			     ;;Last elem will not be processed by 'reduce'.
			     (list 'whateva))
		     :initial-value (cons 0 "\n")))
	  ;;Insert links
	  (insert (concat "- [[file:./" f "][" (file-name-nondirectory f) "]] \n")))))))

(defun n/folder-to-org ()
  "From current directory, creates org buffer containing all files.

Helpful function to create an index file, containing all files in folder hierarchy."
  (let* ((dir default-directory)
	 (out-buf-name "* N/ORG-DIR  *")
	 (out-buf (generate-new-buffer out-buf-name)))
    (message (concat "n/folder-to-org: called with dir: " dir))
    (save-excursion
      (with-current-buffer out-buf
	(org-mode)
	;;(org-insert-heading nil nil t)
	(n/folder-to-org-inserts dir)
	)
      ;;Switch to buffer and hide headlines.
      (switch-to-buffer out-buf)
      ;;Note: Is this how you overwrite values in elisp? Dynamic binding?
      (let ((org-startup-folded t))
	(org-set-startup-visibility))
      (goto-char (point-min)))))

(defun n/folder-to-org-file (&optional directory open-file-afterwards)
  "From current directory, creates org file named index.org (or index_folder_to_org_file.org) containing 
links to all files. Helpful function to create an index file, containing all files in folder hierarchy.

Note: Generated index file is going to have a specific org-title.
It will not overwrite index.org if title is not the same (it will write instead to index_folder_to_org_file.org).

Options: If no directory is given, takes directory of current buffer. Set open-file-afterwards to open-file in buffer."
  (let* ((dir (or directory default-directory))
	 (prelim-index-file (concat dir "index.org"))
	 (plan-b-index-file (concat dir "index_folder_to_org_file.org"))
	 (first-line "#+TITLE: Index (generated by elisp-fn n/folder-to-org-file)")
	 (index-file (if (or (not (file-exists-p prelim-index-file))
			      ;;It file already exists, then first line should be ours.
			      (n/match-a-line-in-file prelim-index-file first-line 0))
			 prelim-index-file
		       plan-b-index-file
		       )))
    (message (concat "n/folder-to-org-file: called with dir: " dir "   --  filename: " index-file))
    (with-temp-file index-file
      (insert first-line)
      (insert "\n")
      (insert (concat "*" " " "Files at root " "\n"))
      (org-mode)
      (n/folder-to-org-inserts dir))
    ;; Option: open file
    (when open-file-afterwards
      (find-file index-file))
    ))
;; Creating org buffer with links to all files in folders:1 ends here

;; [[file:../config.org::*Org publish convenient functions][Org publish convenient functions:1]]
;; ----------------------------------------------
;;   ___                              _     _ _     _        __           
;;  / _ \ _ __ __ _       _ __  _   _| |__ | (_)___| |__    / _|_ __  ___
;; | | | | '__/ _` |_____| '_ \| | | | '_ \| | / __| '_ \  | |_| '_ \/ __|
;; | |_| | | | (_| |_____| |_) | |_| | |_) | | \__ \ | | | |  _| | | \__ \
;;  \___/|_|  \__, |     | .__/ \__,_|_.__/|_|_|___/_| |_| |_| |_| |_|___/
;;            |___/      |_|
;; ----------------------------------------------


;; --------  Tools for variable org-publish-project-alist


(defun n-get-plist-keys--inner-fn (plist &optional acc)
  (if (eq nil plist)
      acc
    (n-get-plist-keys--inner-fn (cddr plist) (cons (car plist) acc))))

(defun n-get-plist-keys (plist)
  "Returns keys of plist. A plist is not an assoc list.
A plist example: (list :option \"something\" :option2 'whatev )"
  (if (n-even? (length plist))
      (n-get-plist-keys--inner-fn plist nil)
    (progn (print "n-get-plist-keys: Did not received a plist:")
	   (print plist)
	   (error "n-get-plist-keys: received not a plist"))))

(defun n-org-publish-get-elem-in-plist (plist prop)
  "Given an element of org-publish-project-alist (plist), will
  return the value of property."
  (if (eq nil plist) (progn (warn (concat "n-org-publish-get-elem-in-plist: plist does not contain prop: " prop)) nil)
    (if (equal prop (car plist))
	(progn ;;(message (concat "n-org-publish-get-elem-in-plist result: " (cadr plist)))
	       (cadr plist))
      (n-org-publish-get-elem-in-plist (cdr plist) prop)
      )))

;; --------- Convenient org-publish wrapper


(defun n-org-publish-get-some-projNames (regex-filter)
  "Return list with all proj names from org-publish-project-alist that do not start with REGEX-FILTER."
  (n-filter #'(lambda (x) (not (string-match regex-filter x 0)))
	      (mapcar #'(lambda (xs) (car xs)) org-publish-project-alist)))


(defun n-org-publish (prefix-arg projName)
  "Will call org-publish asking for project name. All projects which names start 
with \"--\" will be ignored (convenience function).

If at least one prefix arg, it will build an org-publish-alist
from all projs in n-org-pprojs and org-publish-project-alist
itself.

One prefix arg: force publish. Two: async.
Three: Does not evaluate babel supported code snippets (does not ask for it).
"
  (interactive 
    (list current-prefix-arg 
          (ido-completing-read "Pick proj to publish: "
	        (n-org-publish-get-some-projNames "^--")
                nil t)))
  (cond ((equal prefix-arg nil) (org-publish projName))
	;; force
        ((equal prefix-arg '(4)) (org-publish projName t))
	;; force + async
        ((equal prefix-arg '(16)) (org-publish projName t t))
	((equal prefix-arg '(64)) (let ((org-export-use-babel nil))
				    (progn (message "n-org-publish: Will not evaluate babel code.")
					   (org-publish projName))))
	((equal prefix-arg '(256)) (let ((org-export-use-babel nil))
				    (progn (message "n-org-publish: Will not evaluate babel code.")
					   (org-publish projName t))))
	((equal prefix-arg '(1024)) (message (concat "Test prefix arg." projName)))
        (t (warn "n-org-publish: Too many prefix-args (C-u's) given."))))



(defun n-org-publish-force ()
  (interactive)
  (let ((current-prefix-arg '(4))) (call-interactively 'n-org-publish)))

(defun n-org-publish-force-and-async ()
  (interactive)
  (let ((current-prefix-arg '(16))) (call-interactively 'n-org-publish)))

(defun n-org-publish-no-code-eval ()
  (interactive)
  (let ((current-prefix-arg '(64))) (call-interactively 'n-org-publish)))

(defun n-org-publish-force-no-code-eval ()
  (interactive)
  (let ((current-prefix-arg '(256))) (call-interactively 'n-org-publish)))
;; Org publish convenient functions:1 ends here

;; [[file:../config.org::*Provide][Provide:1]]
(provide 'n-elisp-tools-org-mode)
;; Provide:1 ends here
