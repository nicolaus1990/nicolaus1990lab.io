;;; package --- Summary
;;; Commentary:
;;; Code:

;;-- Vars

(setq n/folder-personal-base "/home/nm/Documents/Personal/Blog/")

;;-- Dependencies

(add-to-list 'load-path (concat n/folder-personal-base "src_elisp_blog_maker/"))

(require 'n-elisp-tools-general) ;; For fn n/get-string-from-file

;;TODO: rename to n-org-blog
(require 'n-org-publish-helper-fns)


;;----------------------------------------------------------------------------
;;   ___  ____   ____                    __ _
;;  / _ \|  _ \ / ___|   ___ ___  _ __  / _(_)
;; | | | | |_) | |  _   / __/ _ \| '_ \| |_| |/ _` |
;; | |_| |  _ <| |_| | | (_| (_) | | | |  _| | (_| |
;;  \___/|_| \_\\____|  \___\___/|_| |_|_| |_|\__, |
;;                                            |___/
;;----------------------------------------------------------------------------



;; (require 'org)

(setq org-src-fontify-natively t
      org-export-with-toc nil
      org-export-with-section-numbers nil
      ;;org-export-use-babel nil ;; To avoid evaluation of code (latex or any babel supported lang). Better not set this globally to nil.
      ;;org-export-with-drawers '(not "LOGBOOK" "FEEDSTATUS")
      ;; Will continue exporting, and mark broken links with: "[BROKEN LINK: path]"
      org-export-with-broken-links 'mark
      )


;; (org-babel-do-load-languages
;;  'org-babel-load-languages
;;  '((emacs-lisp . t)
;;    (org . t)
;;    (clojure . t)
;;    (python . t)
;;    (java . t)
;;    (C . t)
;;    (shell . t)
;;    ))


;;----------------------------------------------------------------------------
;;  ____  _                                __ _
;; | __ )| | ___   __ _    ___ ___  _ __  / _(_)
;; |  _ \| |/ _ \ / _` |  / __/ _ \| '_ \| |_| |/ _` |
;; | |_) | | (_) | (_| | | (_| (_) | | | |  _| | (_| |
;; |____/|_|\___/ \__, |  \___\___/|_| |_|_| |_|\__, |
;;                |___/                         |___/
;;----------------------------------------------------------------------------


(let* ((base (if (boundp 'n/folder-personal-base)
		 n/folder-personal-base
               "/home/nm/Documents/Personal/Blog/"))
       (root (concat base "src/"))
       (out (concat base "target/"))
       (posts (concat root "posts/"))
       (drafts (concat root "drafts/"))
       (resources (concat root "resources/"))
       (drafts (concat root "drafts/"))
       ;;HTML Headers, preambles, and postambles
       (hsf (concat root "html-snippets/")))
  (setq n/my-blog-root-dir root
	n/my-blog-target-dir out
	n/my-blog-posts-dir-src posts
	n/my-blog-posts-dir-target (concat out "posts/")
	n/my-blog-resources-dir-src resources
	n/my-blog-resources-dir-target (concat out "resources/")
	n/my-blog-drafts-dir-src drafts
	n/my-blog-drafts-dir-target (concat out "drafts/")
	n/my-blog-html-snippet-header-file (concat hsf "excerpt_page_header.html")
	n/my-blog-html-snippet-preamble-file (concat hsf "excerpt_page_preamble.html")
	n/my-blog-html-snippet-postamble-file (concat hsf "excerpt_page_postamble.html")
	n/my-blog-html-snippet-header (concat hsf "template_header.html")
	n/my-blog-html-snippet-preamble (concat hsf "template_preamble.html")
	n/my-blog-html-snippet-postamble (concat hsf "template_postamble.html")
	))


(setq n-blog-vars `((title "N-blog")
		    (url "https://nicolaus1990.gitlab.io/")
		    (description "My personal blog.")
		    (lang-code "en")
		    (html-head-path-for-gen-htmls ,n/my-blog-html-snippet-header)
		    (html-preamble-path-for-gen-htmls ,n/my-blog-html-snippet-preamble)))



(setq n/my-blog-plist-name "n-blog")
(setq n/my-blog-plist
      `(,n/my-blog-plist-name
	   ;; Note: these pages will not be in archives.html. They have to be
	   ;; manually linked to the home page (or some other page).
	   :base-directory ,n/my-blog-root-dir
	   :base-extension "org"
	   ;; NOTE: Separate fns are going to generate an archive.html and
	   ;; tags.html (and also rss.xml). Don't make an org-file at root level
	   ;; with those names. 
	   :exclude ,n/blog-regex-match-not-allowed 
	   :recursive t 
	   :publishing-directory ,n/my-blog-target-dir
	   :publishing-function org-html-publish-to-html
	   :section-numbers nil
	   ;;:with-toc t ;; Set individually on file with: #+OPTIONS: :toc 4
	   :preparation-function ,(list '(lambda (plist) (print "*** Starting prep fns ***"))
					'n/blog-set-up-vars
					'n/blog-create-tags-org-file)
	   ;; Completion function (rss + multi-page)
	   :completion-function (;; n-org-publish-call-n/blog-fns
				 n/blog-assemble-archive
				 n/blog-assemble-rss 
				 )
	   ;; Note: :preparation-function option is not working. Thus we load snippets like this:
	   :html-head ,(n/get-string-from-file n/my-blog-html-snippet-header-file)
	   :html-preamble (lambda (plist) (n/blog-insert-html-preamble n/my-blog-html-snippet-preamble
								       plist))
	   ;;:html-preamble ,(n/get-string-from-file n/my-blog-html-snippet-preamble-file)
	   ;; :html-head-extra
	   ;; "<link rel=\"alternate\" type=\"application/rss+xml\"
	   ;;        href=\"http://nicolaus1990.gitlab.com/rss.xml\"
	   ;;        title=\"RSS feed\">"
	   ;; We're going to call a fn that will generate rss, archive
	   ;; (index.html) and tags page (fns from org-static-blog minor mode),
	   ;; after completing this plist. These fns should only be called after
	   ;; posts are generated (rss fetches some information in html's of
	   ;; posts).
	   ;;
	   :n-blog-vars ,n-blog-vars
	   ;; :html-head-path-for-gen-htmls ,n/my-blog-html-snippet-header 
	   ;; :html-preamble-path-for-gen-htmls ,n/my-blog-html-snippet-preamble 
	   ))

(setq n/my-blog-with-drafts-plist-name "n-blog-with-drafts")
(setq n/my-blog-with-drafts-plist
      `(,n/my-blog-with-drafts-plist-name
	   :base-directory ,n/my-blog-root-dir
	   :base-extension "org"
	   :exclude ,n/blog-regex-match-not-allowed-except-drafts
	   :recursive t 
	   :publishing-directory ,n/my-blog-target-dir
	   :publishing-function org-html-publish-to-html
	   :section-numbers nil
	   ;;:with-toc nil
	   :preparation-function ,(list '(lambda (plist) (print "*** Starting prep fns ***"))
					'n/blog-set-up-vars
					'n/blog-create-tags-org-file)
	   :completion-function (n/blog-assemble-archive)
	   :html-head ,(n/get-string-from-file n/my-blog-html-snippet-header-file)
	   :html-preamble (lambda (plist) (n/blog-insert-html-preamble n/my-blog-html-snippet-preamble
								       plist))
	   :n-blog-vars ,n-blog-vars
	   ;; :html-head-path-for-gen-htmls ,n/my-blog-html-snippet-header 
	   ;; :html-preamble-path-for-gen-htmls ,n/my-blog-html-snippet-preamble 
))

(setq n/my-blog-resources-plist-name "--n-blog--resources")
(setq n/my-blog-resources-plist
      `(,n/my-blog-resources-plist-name 
	:base-directory ,n/my-blog-root-dir ;;,n/my-blog-resources-dir-src
	:base-extension ,n/blog-regex-extensions-allowed
	:publishing-directory ,n/my-blog-target-dir ;;,n/my-blog-resources-dir-target
	:recursive t
	:exclude "/#\\|target/*" 
	:publishing-function org-publish-attachment))

(setq n/my-blog-with-drafts
      `("blogN-with-drafts"
	:components (,n/my-blog-with-drafts-plist-name 
		     ,n/my-blog-resources-plist-name)))
(setq n/my-blog-to-publish
      `("blogN"
	:components (,n/my-blog-plist-name
		     ,n/my-blog-resources-plist-name)))


(defun n/personal-blog (&optional force with-drafts)
  "Wrapper for n/blog-org-publish."
  (setq n/org-blog-publish-project-alist (list
					  ;; Blog plists:
					  n/my-blog-plist
					  n/my-blog-with-drafts-plist
					  n/my-blog-resources-plist
					  n/my-blog-to-publish))
  (n/blog-org-publish  (if with-drafts
			   (progn (message "Drafts included.") n/my-blog-with-drafts)
			 (progn (message "Drafts excluded.") n/my-blog-to-publish))
		       force))

;;(n/blog-org-publish)
;;(n/blog-org-publish t t)


