;; [[file:../config.org::*Dependencies and aliases of external fns][Dependencies and aliases of external fns:1]]
;; ----------------------------------------------
;;  ____                            _                 _
;; |  _ \  ___ _ __   ___ _ __   __| | ___ _ __   ___(_)
;; | | | |/ _ \ '_ \ / _ \ '_ \ / _` |/ _ \ '_ \ / __| |/ _ \/ __|
;; | |_| |  __/ |_) |  __/ | | | (_| |  __/ | | | (__| |  __/\__ \
;; |____/ \___| .__/ \___|_| |_|\__,_|\___|_| |_|\___|_|\___||___/
;;            |_|
;; ----------------------------------------------


(require 'seq) ;; for seq-filter
(defalias 'n-filter 'seq-filter)

(require 'cl) ;; for reduce
(defalias 'n-foldl 'reduce)
;; Dependencies and aliases of external fns:1 ends here

;; [[file:../config.org::*Numeric][Numeric:1]]
;; -------------------------------------------------------------
;;  _   _                           _
;; | \ | |_   _ _ __ ___   ___ _ __(_) ___
;; |  \| | | | | '_ ` _ \ / _ \ '__| |/ __|
;; | |\  | |_| | | | | | |  __/ |  | | (__
;; |_| \_|\__,_|_| |_| |_|\___|_|  |_|\___|
;; -------------------------------------------------------------


(defun n-even? (num) (= (% num 2) 0))
(defun n-odd? (num) (= (% num 2) 1))
;; Numeric:1 ends here

;; [[file:../config.org::*Datastructure stuff][Datastructure stuff:1]]
;; -------------------------------------------------------------
;;  ____        _              _                   _                     __           
;; |  _ \  __ _| |_ __ _   ___| |_ _ __ _   _  ___| |_ _   _ _ __ ___   / _|_ __  ___ 
;; | | | |/ _` | __/ _` | / __| __| '__| | | |/ __| __| | | | '__/ _ \ | |_| '_ \/ __|
;; | |_| | (_| | || (_| | \__ \ |_| |  | |_| | (__| |_| |_| | | |  __/ |  _| | | \__ \
;; |____/ \__,_|\__\__,_| |___/\__|_|   \__,_|\___|\__|\__,_|_|  \___| |_| |_| |_|___/
;; -------------------------------------------------------------

(defun n/add-listelems (target-list from-list)
  ;; example: 
  ;;    (setq n/someList '(1 2 3))
  ;;    (n/add-listelems 'n/someList '(4 5 6)) --> returns some random list,but...
  ;;    n/someList --> (6 5 4 1 2 3)
  ;;(mapcar '(lambda (elm) (push elm (symbol-value target-list))) from-list)
  ;; Not returning anything:
  (mapc #'(lambda (elm) (push elm (symbol-value target-list))) from-list)
)


(defun n/remove-dups (xs)
  "Remove duplicates. Functional. Requires cl lib."
  ;; Example of reduce:
  ;; (n-foldl '(lambda (x y) (if (member y x) x (cons y x)))
  ;;          (list "a" "b" "c" "a" "b" "d")
  ;;	      :initial-value nil)
  (n-foldl '(lambda (x y) (if (member y x) x (cons y x)))
           xs
	   :initial-value nil))


;; Abstraction tools
;; Establishes the next index of the item in the vector (set) to enable.
(defun n/next-index-for-cycle (fn-name-as-symbol numItems)
  "@Output Integer: Sets 'fn-name to the next index
   ie: uses a property 'state' (integer)
   Note: fn-name-as-symbol must be the name of the function."
  (if (get fn-name-as-symbol 'state)
      (let* ((current-index (get fn-name-as-symbol 'state))
	     (next-index (% (+ current-index numItems 1) numItems)))
        (progn
          (put fn-name-as-symbol 'state next-index)
          next-index))
      (progn
        (put fn-name-as-symbol 'state 0)
        0)))
;; Datastructure stuff:1 ends here

;; [[file:../config.org::*String and regex manipulation][String and regex manipulation:1]]
;;; -------------------------------------------------------------
;;  ____  _           __ 
;; / ___|| |_ _ __   / _|_ __  ___
;; \___ \| __| '__| | |_| '_ \/ __|
;;  ___) | |_| |    |  _| | | \__ \
;; |____/ \__|_|    |_| |_| |_|___/
;;; -------------------------------------------------------------                                 

;; String manipulation (Finding text, etc)
;; Helper functions: 
(defun n/string/starts-with (string prefix)
      "Return t if STRING starts with prefix."
      (and (string-match (rx-to-string `(: bos ,prefix) t)
                         string)
           t))

(defun n/replace-in-string (what with in)
  ;;(n/replace-in-string ".." "../" "....")
  (replace-regexp-in-string (regexp-quote what) with in nil 'literal))
;; String and regex manipulation:1 ends here

;; [[file:../config.org::*I/O files and directory fns][I/O files and directory fns:1]]
;;; -------------------------------------------------------------
;;  ____       _   _              __ _ _               ___    _____ 
;; |  _ \ __ _| |_| |__  ___     / _(_) | ___  ___    |_ _|  / / _ \
;; | |_) / _` | __| '_ \/ __|   | |_| | |/ _ \/ __|    | |  / / | | |
;; |  __/ (_| | |_| | | \__ \_  |  _| | |  __/\__ \_   | | / /| |_| |
;; |_|   \__,_|\__|_| |_|___( ) |_| |_|_|\___||___( ) |___/_/  \___/
;;                          |/                    |/
;;; -------------------------------------------------------------

(defun n-get-folder-level-down (root file)
  "Given file-path FILE and path ROOT, and return number of folders lower than ROOT (requires: ROOT is subpath of FILE).

Example: 
(n-get-folder-level-down \"/home/nm/Documents/Personal/Blog/src/\"
                         \"/home/nm/Documents/Personal/Blog/src/html-snippets/preamble.org\")
--> 1"
  ;; To test: (n-get-folder-level-down "/home/nm/Documents/Personal/Blog/src/" "/home/nm/Documents/Personal/Blog/src/html-snippets/s/preamble.org")
  (progn ;;(print root) (print file)
    (if (not (eq 0 (string-match-p (regexp-quote root) file)))
	;; To test: (= 0 (string-match-p (regexp-quote "a/") "a/sja/"))
	(progn (warn (concat "n-get-folder-level-down: root is not part of path of file. "
			     "root: " root " file: " file ))
	       0)
      (let ((file-path-relative (substring file (length root))))
	(- (length (split-string file-path-relative "/")) 1)
	;;To test:(- (length (split-string "html-snippets/preamble.org" "/")) 1)
	;;        (- (length (split-string "preamble.org" "/")) 1)
	))))


(defun n/insert-str-2-str (str-to-append prefix-str)
  "Insert string after every occurrence of prefix-str in file.
For example: (n/insert-str-to-str \"[#A]\" \"* INPROGRESS \""
  (save-mark-and-excursion
    (goto-char (point-min))
    (while (search-forward prefix-str nil t)
      (insert str-to-append " ")
      )
    ))

;;; To get filename:
;; (file-name-nondirectory "lewis/foo")
;;; To get path of file without filename:
;;   (file-name-directory "/some/path/to/my/source.ext")
;;; To get folder of file, without last slash:
;;   (directory-file-name
;;     (file-name-directory "/some/path/to/my/source.ext"))
;;; To get folder of file, only parent folder's name:
;; (file-name-nondirectory
;;   (directory-file-name
;;     (file-name-directory "/some/path/to/my/source.ext"))) 


(defun n/get-string-from-file (filePath)
  "Return filePath's file content."
  ;; thanks to “Pascal J Bourguignon” and “TheFlyingDutchman 〔zzbba...@aol.com〕”. 2010-09-02
  (with-temp-buffer
    (insert-file-contents filePath)
    (buffer-string)))

(defun n/match-a-line-in-file (file str &optional line-num)
  "Will try to match STR to the line in file. If no line-num is given, will match the firs line"
  (let* ((l-num (when line-num 0))
	 (line-to-compare nil)
	 (ans nil))
    (progn
      (with-temp-buffer
	(insert-file-contents file)
	(goto-char (point-min))
	(forward-line l-num)
	;; From xah (http://ergoemacs.org/emacs/elisp_all_about_lines.html): avoid thing-at-point,
	;;(thing-at-point 'line)
	;; to grab current line, better use (buffer-substring-no-properties ...)
	(setq line-to-compare
	      (buffer-substring-no-properties
		(line-beginning-position)
		(line-end-position)))
	)
      (message (concat "n/match-a-line-in-file: str: --" str "   <-->   --" line-to-compare))
      (equal str line-to-compare))))


(defun n/append-regions (src-file target-file start-tag end-tag)
  "Appends regions of text in a file to another file. Regions are
delimeted with some specified start- and end-tags."
  (let ((t-file (expand-file-name target-file)))
    (save-mark-and-excursion
      (with-temp-buffer
	(insert-file-contents src-file)
	(goto-char (point-min))
	(let ((n-beg-pos nil)
	      (n-end-pos nil))
	  (while (search-forward start-tag nil t)
	    (move-beginning-of-line nil)
	    (setq n-beg-pos (point))
	    (set-mark-command nil)
	    (search-forward end-tag);;throws error if not found
	    (setq n-end-pos (point))
	    ;;(kill-ring-save nil nil t)
	    (append-to-file n-beg-pos n-end-pos t-file)
	    (append-to-file "\n" nil t-file)
	    )))
      )))


(defun n/get-files-sorted (dir)
  "Get all files in dir, sorted, relative path."
  ;;Use (seq-take xs 5) for debugging
  (cl-sort (mapcar #'(lambda (f) (substring f (length dir)))
		   (directory-files-recursively dir ""))
	   ;;; Instead of sorting only by lexico..
	   ;;'string-lessp :key 'downcase
	   ;;; will also group by files not in subfolders.
	   '(lambda (s1 s2)
	      (let ((s1-is-file (not (string-match "/" s1)))
		    (s2-is-file (not (string-match "/" s2))))
		(or
		 (and (or s1-is-file s2-is-file)
		      (not (string-match "/" s1)))
		 (string-lessp s1 s2))))))

;;(seq-take (n/get-files-sorted default-directory) 5)
;; I/O files and directory fns:1 ends here

;; [[file:../config.org::*Miscellaneous (color hex)][Miscellaneous (color hex):1]]
;;; -------------------------------------------------------------
;;  __  __ _              _ _ 
;; |  \/  (_)___  ___ ___| | | __ _ _ __   ___  ___  _   _ ___
;; | |\/| | / __|/ __/ _ \ | |/ _` | '_ \ / _ \/ _ \| | | / __|
;; | |  | | \__ \ (_|  __/ | | (_| | | | |  __/ (_) | |_| \__ \
;; |_|  |_|_|___/\___\___|_|_|\__,_|_| |_|\___|\___/ \__,_|___/
;;; -------------------------------------------------------------                                                             

;;; ----- Color

(defun n/get-current-background-colour ()
   (face-attribute 'default :background))

(defun n/lighten/darken-colour (col &optional pct)
  "Input: String, a colour in hex-format, optional: percent
Default: lightens"
    ;;For pct: or evals args until one of them yields non-nil, then return that value.
    (let ((percent (or pct 5))) 
      (apply 'color-rgb-to-hex
        (apply 'color-hsl-to-rgb
          (apply 'color-lighten-hsl
            (append (apply 'color-rgb-to-hsl
              (color-name-to-rgb col))
              (list percent)))))))
;; Miscellaneous (color hex):1 ends here

;; [[file:../config.org::*Dev: log, among other fns][Dev: log, among other fns:1]]
;; -------------------------------------------------------------
;;  ____                __
;; |  _ \  _____   __  / _|_ __  ___
;; | | | |/ _ \ \ / / | |_| '_ \/ __|
;; | |_| |  __/\ V /  |  _| | | \__ \
;; |____/ \___| \_/   |_| |_| |_|___/
;; -------------------------------------------------------------

(defun n-print-and-ret (something)
  (progn (print something)
	 something))
;; Dev: log, among other fns:1 ends here

;; [[file:../config.org::*Provide][Provide:1]]
;;;;;; Provide
(provide 'n-elisp-tools-general)
;; Provide:1 ends here
