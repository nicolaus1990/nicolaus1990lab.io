;; [[file:../../config.org::*EVERYTHING][EVERYTHING:1]]
;;; n-org-publish-helper-fns.el --- Helping fns for org-publishing and blogging. -*- lexical-binding: f -*-

;; Copyright (C) Nicolaus Möller

;; Author: Nicolaus Möller <notARealE@email.com>
;; Homepage: https://gitlab.com/...?
;; Keywords: org-publish
;; Version: 0.1

;;; Commentary:

;; -----------------------------------------------------------
;;
;; This contains some helper fns for org-publishing.
;;
;; 
;; Many of functions and ideas were shamelessly taken from:
;; https://gitlab.com/_zngguvnf/org-static-blog-example/tree/master .
;; A newer version fo org-static-blog was taken and modified to use org-publish.
;;
;;; -----------------------------------------------------------
;;;
;;; It should contain the following:
;;;      i) Path variables. (Change paths apprapiately).
;;;     ii) Fns taken from org-static-blog minor mode
;;;    iii) Org-publish project list.
;;;
;;; -------------------------------------------------------------


;; ----------------------------------------------
;;  ____                            _                 _
;; |  _ \  ___ _ __   ___ _ __   __| | ___ _ __   ___(_)
;; | | | |/ _ \ '_ \ / _ \ '_ \ / _` |/ _ \ '_ \ / __| |/ _ \/ __|
;; | |_| |  __/ |_) |  __/ | | | (_| |  __/ | | | (__| |  __/\__ \
;; |____/ \___| .__/ \___|_| |_|\__,_|\___|_| |_|\___|_|\___||___/
;;            |_|
;; ----------------------------------------------


;;(require 'cl);; For reduce

(require 'n-elisp-tools-general) ;; For fns n/get-string-from-file, n-filter, n-foldl...

(require 'n-elisp-tools-org-mode)

;(require 'org)
;(require 'seq) ????

(require 'seq) ;; for seq-filter

;; begin org-static-blog (version "1.2.1")
;; https://github.com/bastibe/org-static-blog/tree/53d53b6d02c2369654196c230056630cb21bd8d8

(require 'org)
(require 'ox-html)


;; ----------------------------------------------
;; __     __         _       _     _           
;; \ \   / /_ _ _ __(_) __ _| |__ | | ___  ___ 
;;  \ \ / / _` | '__| |/ _` | '_ \| |/ _ \/ __|
;;   \ V / (_| | |  | | (_| | |_) | |  __/\__ \
;;    \_/ \__,_|_|  |_|\__,_|_.__/|_|\___||___/
;; ----------------------------------------------


(defvar n-org-blog-generated-files-folder-name "n-org-blog-gen/"
  "Folder where generated-files are stored")


(defvar n/blog-tags-file "tags.html"
  "Name of tags file.")

(defvar n/blog-rss-file
  (concat n-org-blog-generated-files-folder-name
	  "rss.xml")
  "File (with path) of rss file.")

(defvar n/blog-archive-file
  (concat n-org-blog-generated-files-folder-name
	  "archive.html")
  "File of archive file.")

(defvar n/org-blog-publish-project-alist 
      nil
      "This variable behaves like org-publish-project-alist.
Setting it, will overshadow org-publish-project-alist in each
 n/blog-org-publish call.")


;; ----------------------------------------------
;;             Data structure
;; ----------------------------------------------

(defvar n-org-publish-attrs--default-vals
  `((title "BLOG_TITLE_MISSING")
    (url "https://BLOG-URL-MISSING")
    (description "DESCRIPTION FOR BLOG MISSING.")
    (lang-code "en")
    (html-head-path-for-gen-htmls ,nil)
    (html-preamble-path-for-gen-htmls ,nil))
  "Default attributes used in  this file (to retrieve use n-org-publish-get).")

;;To test: (n-org-publish-get--inner 'url n-org-publish-attrs--default-vals)

(defvar n-org-publish-attrs nil
  "Attributes used in  this file (to retrieve use n-org-publish-get).

Set this variable preferably by putting :n-blog-vars in the
source plist of org-publish-project-alist and using the fn
n/blog-set-up-vars (see its docs).

Example of n-org-publish-attrs:

(list '(titles \"SOME_TITLE\")
      '(url \"https://SOME_URL\")
      '(description \"Some description\")
      '(lang-code \"en\")
      '(html-head-path-for-gen-htmls ,nil)
      '(html-preamble-path-for-gen-htmls ,nil))

")

(defun n-org-publish-get--inner (prop &optional assoc-list-vars)
  (let ((xss (or assoc-list-vars n-org-publish-attrs)))
    (cadr (assoc prop xss))))



(defun n-org-publish-get (prop)
  "PROP is a property in assoc-list n-org-publish-attrs." 
  (let ((user-given-val (n-org-publish-get--inner prop)))
    (or user-given-val
	;; If user did not set something, retrieve default var:
	(n-org-publish-get--inner prop
				  n-org-publish-attrs--default-vals))))


(defun n/blog-set-up-vars (plist)
  "Retrieves :n-blog-vars assoc list from PLIST and sets variables.

Usage: put this function in :preparation-function of plist in
org-publish-project-alist. And put in plist of
org-publish-project-alist a assoc-list, like this:

`(...
:preaparation-function ,(list 'n/blog-set-up-vars)
:n-blog-vars ((titles \"SOME_TITLE\")
              (url \"https://SOME_URL\")
              (description \"Some description\")
              (lang-code \"en\")
              (html-head-path-for-gen-htmls ,nil)
              (html-preamble-path-for-gen-htmls ,nil))
...)
"
  (print "*** Setting vars and validating (n/blog-set-up-vars) ***")
  (let ((vars (n-org-publish-get-elem-in-plist plist :n-blog-vars)))
    ;;Validate first:
    (if (and vars
	     (assoc 'url vars)
	     (assoc 'title vars)
	     (assoc 'html-preamble-path-for-gen-htmls vars)
	     (assoc 'html-head-path-for-gen-htmls vars))
	(progn (print "*** Validation of variables succesful. ***")
	       (setq n-org-publish-attrs vars))
      (progn (error (concat "n/blog-set-up-vars: plist " plist))))))



;; ----------------------------------------------
;;             Regexes
;; ----------------------------------------------


(defvar n/blog-regex-extensions-allowed 
  ;;Should be something like: "css\\|js\\|png\\|jpg\\|ico\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|woff2\\|ico"
  ;; To test: (string-match n/blog-regex-extensions-allowed  "sss/drafts/safag.jspgs")
  ;; (setq n/blog-regex-extensions-allowed) 
  (let ((ext-allowed (list "css" "js" "png" "jpg" "ico" "gif" "ttf" "wav"
			   "pdf" "mp3" "ogg" "swf" "woff" "woff2" "ico")))
    
    (n-foldl '(lambda (x y) (concat x "\\|" y))
	     (mapcar #'(lambda (ext) (concat ext "$"))
		     (mapcar 'regexp-quote
			     ;; Do not add dot! Regex is used in
			     ;; org-publish-project-alist :base-extension
			     ;; option, which does require the exclusion of the dot..
			     ;;(mapcar #'(lambda (ext) (concat "." ext)))
			     ext-allowed))))
    "The extensions of resources to allow.")


;; TODO: Use 'rx' for creating regex (easier to maintain).
(defvar n/blog-regex-match-not-allowed 
  ;;Should be something like: "/#\\|/xxx\\|/zz\\|README.org\\|archive.org"
  ;; To test: (string-match n/blog-regex-match-not-allowed "sss/html-fsnippets/draftsss/safag")
  ;;(setq n/blog-regex-match-not-allowed) 
  (let ((regexes (list "drafts/" "html-snippets/"
		       ;;Do not put first slash. Note: this regex is going to
		       ;;be used in org-publish-project-alist option
		       ;;:exclude... and apparantly, path comes without it when
		       ;;matching. So this: "/drafts/" will not match!
		       "/#" "/xxx" "/zz" "/ignore"
		       (regexp-quote ".git/")
		       )))
    (n-foldl '(lambda (x y) (concat x "\\|" y))
	     regexes))
  "The extensions of files to be ignored.")

(defvar n/blog-regex-match-not-allowed-except-drafts
  ;;Should be something like: "/#\\|/xxx\\|/zz\\|README.org\\|archive.org"
  ;; To test: (string-match n/blog-regex-match-not-allowed-except-drafts "sss/html-fsnippets/draftsss/safag")
  ;;(setq n/blog-regex-match-not-allowed-except-drafts) 
  (let ((regexes (list "html-snippets/"
		       ;;Do not put first slash. Note: this regex is going to
		       ;;be used in org-publish-project-alist option
		       ;;:exclude... and apparantly, path comes without it when
		       ;;matching. So this: "/html-snippets/" will not match!
		       "/#" "/xxx" "/zz" "/ignore"
		       (regexp-quote ".git/")
		       )))
    (n-foldl '(lambda (x y) (concat x "\\|" y))
	     regexes))
  "The extensions of files to be ignored... but allow files in drafts folder.")


;; ----------------------------------------------
;;  _                _ 
;; | |    ___   __ _(_) ___
;; | |   / _ \ / _` | |/ __|
;; | |__| (_) | (_| | | (__ 
;; |_____\___/ \__, |_|\___|
;;             |___/
;; ----------------------------------------------


(defun n-org-publish--is-draft? (file-name)
  (or (string-match-p "/draft[s]?/" file-name)
      ;;(string-match-p (regexp-quote "/draft/") file-name)
      ;; To test: (string-match-p "/draft[s]?/" "hello/drafts/")
      ))


(defun n/blog-gen-html-filename (post-filename &optional base-dir relative-to-root-dir)
  "Generate HTML file name for POST-FILENAME.
RELATIVE-TO-ROOT-DIR should be number: the number of folders up relative to base-dir path."
  (concat ;;(if relative-to-root-dir "" (n-org-publish-get nil 'target-dir) )
   ;; Previously, org-static-blog generated all html files in root folder.
   ;; We've got two more folders: 'posts' and 'drafts' folder.
   ;;(if post-or-draft (if (eq post-or-draft 'post) "posts/" "drafts/") "")
   ;;(substr base-dir post-filename)
   
   ;; Add as many "../" as relative-to-root-dir says.
   (if (not (numberp relative-to-root-dir)) ""
     (n/replace-in-string ".."
			  "../"
			  (make-string (* 2 relative-to-root-dir) ?.)))
   ;; If no base-dir then return only filename without extension, else...
   (if (not base-dir)
       (file-name-base post-filename)
     ;; substract base-dir path from post-filename and also substract extension.
     (file-name-sans-extension (substring post-filename (length base-dir))))
   ;;(file-name-base post-filename) ; (substring "bcdaxyz" (length "bcd"))
   ;; (file-name-base "/home/nm/Documents/Personal/Blog/src/posts/Hello_cyberweb.org")
   ".html"))

(defun n/blog-get-post-filenames (posts-dir &optional regex-match-not-allowed)
  "Returns a list of all posts."
  ;;(directory-files posts-dir t ".*\\.org$" nil)
  (let ((regex-filter  (or regex-match-not-allowed
			   n/blog-regex-match-not-allowed)))
    (n-filter #'(lambda (pf) (not (string-match regex-filter pf)))
	      (directory-files-recursively posts-dir ".*\\.org$"))))

;;TODO: seq-filter --> n-filter
(defun n/blog-file-buffer (file)
  "Return the buffer open with a full filepath, or nil."
  (car (seq-filter
         (lambda (buf)
           (string= (with-current-buffer buf buffer-file-name) file))
         (buffer-list))))

;; This macro is needed for many of the following functions.
(defmacro n/blog-with-find-file (file contents &rest body)
  "Executes BODY in FILE. Use this to insert text into FILE.
The buffer is disposed after the macro exits (unless it already
existed before)."
  `(save-excursion
     (let ((current-buffer (current-buffer))
           (buffer-exists (n/blog-file-buffer ,file))
           (result nil)
	   (contents ,contents))
       (if buffer-exists
           (switch-to-buffer buffer-exists)
         (find-file ,file))
       (erase-buffer)
       (insert contents)
       (setq result (progn ,@body))
       (basic-save-buffer)
       (unless buffer-exists
         (kill-buffer))
       (switch-to-buffer current-buffer)
       result)))


(defun n/blog-get-date (post-filename)
  "Extract the `#+date:` from POST-FILENAME as date-time."
  (let ((case-fold-search t))
    (with-temp-buffer
      (insert-file-contents post-filename)
      (goto-char (point-min))
      (if (search-forward-regexp "^\\#\\+date:[ ]*<\\([^]>]+\\)>$" nil t)
	  (date-to-time (match-string 1))
	nil;;(time-since 0)
	))))

(defun n/blog-get-title (post-filename)
  "Extract the `#+title:` from POST-FILENAME."
  ;;NOTE: post-filename is always a post, no need checking or modifying anything.
  (let ((case-fold-search t))
    (with-temp-buffer
      (insert-file-contents post-filename)
      (goto-char (point-min))
      (if (search-forward-regexp "^\\#\\+title:[ ]*\\(.+\\)$" nil t)
	  (match-string 1)
	nil))))


(defun n/blog-get-url (post-filename &optional base-dir relative-to-root-dir)
  "Generate a URL to the published POST-FILENAME."
  ;(file-name-nondirectory (n/blog-gen-html-filename post-filename t))
  (n/blog-gen-html-filename post-filename
					     base-dir
					     relative-to-root-dir))


(defun n/blog-get-body (post-filename &optional exclude-title)
  "N: Get the org file, try to skip org-file-configs and get body. It would be 
better to have it in html format, but it had to be changed because posts aren't 
being generated with org-static-blog fns anymore (now, with org-publish).

OLD DOC: Get the rendered HTML body without headers from
POST-FILENAME. Preamble and Postamble are excluded, too."
  (with-temp-buffer
    (insert-file-contents post-filename)
    (buffer-substring-no-properties
     (progn
       (goto-char (point-min))
       (search-forward "* ")
       (point))
     (progn
       (goto-char (point-max))
       (point)))))

(defun n/blog-get-matching-publish-filename (plist postfilename)
  "Given an org post POSTFILENAME and PLIST, returns the html
publishing html file."
  (let ((base-dir (n-org-publish-get-elem-in-plist plist :base-directory))
	(publish-target (n-org-publish-get-elem-in-plist plist :publishing-directory))
	(index-of-extension (string-match-p "\\.org$" postfilename)))
    (if (or (not (eq 0 (string-match-p (regexp-quote base-dir) postfilename)))
	    ;; Does not have org extension at the end.
	    (not index-of-extension))
	(progn (warn (concat "n/blog-get-matching-publish-filename: error! "
			     "postfilename: " postfilename " base-dir: " base-dir ))
	       nil)
	(concat publish-target
		(substring postfilename
			   (length base-dir)
			   index-of-extension)
		".html"))))

(defun n/blog-get-html-body (html-post-filename &optional exclude-title)
  "Get the rendered HTML body without headers from
HTML-POST-FILENAME. Preamble and Postamble are excluded, too."
  (with-temp-buffer
    (insert-file-contents html-post-filename)
    (buffer-substring-no-properties
     (progn
       (goto-char (point-min))
       (if exclude-title  
           (progn (search-forward "<h1 class=\"title\">";;"<h1 class=\"post-title\">"
				  )
                  (search-forward "</h1>"))
         (search-forward "<div id=\"content\">"))
       (point))
     (progn
       (goto-char (point-max))
       (search-backward "<div id=\"postamble\" class=\"status\">")
       (search-backward "</div>")
       (point)))))



(defun n/blog-get-draft-filenames ()
  "Returns a list of all drafts."
  (directory-files-recursively
   n/blog-drafts-directory ".*\\.org$"))

;; ----------------------------------------------
;;  _____                                    __ _ _
;; |_   _|_ _  __ _ ___    ___  _ __ __ _   / _(_) | ___
;;   | |/ _` |/ _` / __|  / _ \| '__/ _` | | |_| | |/ _ \
;;   | | (_| | (_| \__ \ | (_) | | | (_| | |  _| | |  __/
;;   |_|\__,_|\__, |___/  \___/|_|  \__, | |_| |_|_|\___|
;;            |___/                 |___/
;; ----------------------------------------------


(defun n/blog-get-tags (post-filename)
  "Extract the `#+filetags:` from POST-FILENAME as list of strings."
  (let ((case-fold-search t))
    (with-temp-buffer
      (insert-file-contents post-filename)
      (goto-char (point-min))
      (if (search-forward-regexp "^\\#\\+filetags:[ ]*:\\(.*\\):$" nil t)
          (split-string (match-string 1) ":")
	(if (search-forward-regexp "^\\#\\+filetags:[ ]*\\(.+\\)$" nil t)
            (split-string (match-string 1))
	  )))))

(defun n/blog-get-tag-tree (post-filenames)
  "Return an association list of tags to filenames.
e.g. `(('foo' 'file1.org' 'file2.org') ('bar' 'file2.org'))`"
  (let ((tag-tree '()))
    (dolist (post-filename post-filenames)
      (let ((tags (n/blog-get-tags post-filename)))
        (dolist (tag tags)
          (if (assoc-string tag tag-tree t)
              (push post-filename (cdr (assoc-string tag tag-tree t)))
            (push (cons tag (list post-filename)) tag-tree)))))
    tag-tree))


(defun n/blog-assemble-tags-archive-tag (base-dir tag)
  "Assemble single TAG for all filenames."
  (let ((post-filenames (cdr tag)))
    (setq post-filenames
	  (sort post-filenames (lambda (x y) (time-less-p (n/blog-get-date x)
						     (n/blog-get-date y)))))
    (concat "*** " (downcase (car tag)) "\n"
	    (apply 'concat (mapcar '(lambda (pf)
				      (n/blog-get-post-summary-in-org-format base-dir pf))
				   post-filenames)))))



(defun n/blog-create-tags-org-file (plist)
  (print "** Trying to create tags org-file... **")
  (let* ((base-dir (n-org-publish-get-elem-in-plist plist :base-directory))
	 (post-filenames (n/blog-get-post-filenames base-dir
							     (n-org-publish-get-elem-in-plist plist :exclude)))
	 (assoc-list-tag-files (n/blog-get-tag-tree post-filenames)))
    (n/blog-with-find-file
     (concat (n-org-publish-get-elem-in-plist plist :base-directory)
	     n-org-blog-generated-files-folder-name
	     "tags.org")
     ;;tags-archive-filename
     (concat
      "#+title: Tags" "\n"
      "#+OPTIONS: :toc 4" "\n"
      ;;TODO: hardcoded date
      "#+date: <2020-12-09 12:00>" "\n"
      "#+HTML_HEAD_EXTRA: <link href=\"../resources/style.css\" rel=\"stylesheet\" type=\"text/css\"/>" "\n"
      "#+HTML_HEAD_EXTRA: <link rel=\"icon\" href=\"../resources/favicon.ico\">" "\n"
      "#+HTML_HEAD_EXTRA: <link rel=\"apple-touch-icon-precomposed\" href=\"../resources/apple-touch-icon.png\">""\n"
      "#+HTML_HEAD_EXTRA: <link rel=\"apple-touch-icon-precomposed\" href=\"../resources/favicon-152.png\">" "\n"
      "#+HTML_HEAD_EXTRA: <link rel=\"msapplication-TitleImage\" href=\"../resources/favicon-144.png\">" "\n"
      "#+HTML_HEAD_EXTRA: <link rel=\"msapplication-TitleColor\" href=\"#0141ff\">" "\n"

      "** Tags" "\n"
      ;; Contents:
      (apply 'concat
	     (mapcar '(lambda (tag-and-files)
			(n/blog-assemble-tags-archive-tag base-dir
								   tag-and-files))
		     assoc-list-tag-files))
      ))))




;; ----------------------------------------------
;;  ____  ____ ____     __ 
;; |  _ \/ ___/ ___|   / _|_ __  ___
;; | |_) \___ \___ \  | |_| '_ \/ __|
;; |  _ < ___) |__) | |  _| | | \__ \
;; |_| \_\____/____/  |_| |_| |_|___/
;; ----------------------------------------------

(defun n/blog-get-complete-rss-url ()
  (concat (n-org-publish-get 'url)
	  n/blog-rss-file))

(defun n/blog-assemble-rss (plist)
  "Assemble the blog RSS feed.
The RSS-feed is an XML file that contains every blog post in a
machine-readable format."
  (let* ((rss-filename (concat (n-org-publish-get-elem-in-plist plist :publishing-directory)
			       n/blog-rss-file))
	 (base-dir (n-org-publish-get-elem-in-plist plist :base-directory))
	 (posts-files (n/blog-get-post-filenames base-dir
							  (n-org-publish-get-elem-in-plist plist :exclude)))
	 (rss-items nil))
    (print "*** Trying to create RSS xml-file... *** ")
    ;;(print posts-files)
    (dolist (post-filename posts-files)
      ;; (print (concat "post-filename: " post-filename))
      ;; (print (concat   "\n Title: "(n/blog-get-title post-filename)))
      ;; (print (concat "\n Body: " (n/blog-get-body post-filename t)))
      ;; (print (concat "\n file-name0: " (n/blog-get-matching-publish-filename plist post-filename)))
      (let* ((rss-date (n/blog-get-date post-filename))
	     (html-post-filename (n/blog-get-matching-publish-filename plist post-filename))
	     (html-content (n/blog-get-html-body html-post-filename t))
	     (link-to-html-file (concat (n-org-publish-get 'url)
					(n/blog-gen-html-filename post-filename
										   base-dir
										   nil)))
             (rss-text (n/blog-get-rss-item post-filename
						     html-content
						     link-to-html-file)))
        (add-to-list 'rss-items (cons rss-date
				      rss-text))))
    ;; (print "------ RSS items: ------ ")
    ;; (print rss-items)
    (setq rss-items (sort rss-items (lambda (x y) (time-less-p (car y) (car x)))))
    (n/blog-with-find-file
     rss-filename
     (concat "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
	     "<rss version=\"2.0\">\n"
	     "<channel>\n"
	     "<title>" (n-org-publish-get 'title) "</title>\n"
	     "<description>" (n-org-publish-get 'description) "</description>\n"
	     "<link>" (n-org-publish-get 'url) "</link>\n"
	     "<lastBuildDate>" (format-time-string "%a, %d %b %Y %H:%M:%S %z" (current-time)) "</lastBuildDate>\n"
	     (apply 'concat (mapcar 'cdr rss-items))
	     "</channel>\n"
	     "</rss>\n"))))

(defun n/blog-get-rss-item (post-filename html-content-description link)
  "Assemble RSS item from post-filename.
The HTML content is taken from the rendered HTML post."
  (let ((post-date (n/blog-get-date post-filename))
	(post-title (n/blog-get-title post-filename)))
    (if (not (and post-date post-title)) ""
      (concat
       "<item>\n"
       "  <title>" post-title "</title>\n"
       "  <description><![CDATA["
       html-content-description ; exclude headline
       "]]></description>\n"
       (let ((categories ""))
	 (when (n/blog-get-tags post-filename)
	   (dolist (tag (n/blog-get-tags post-filename))
	     (setq categories (concat categories
				      "  <category>" tag "</category>\n"))))
	 categories)
       "  <link>" link "</link>\n"
       "  <pubDate>"
       (format-time-string "%a, %d %b %Y %H:%M:%S %z" post-date)
       "</pubDate>\n"
       "</item>\n"))))


;; ----------------------------------------------
;;   ____                               _     _             _     _             _    __ _ _ 
;;  / ___| ___ _ __       __ _ _ __ ___| |__ (_)_   _____  | |__ | |_ _ __ ___ | |  / _(_) | ___
;; | |  _ / _ \ '_ \     / _` | '__/ __| '_ \| \ \ / / _ \ | '_ \| __| '_ ` _ \| | | |_| | |/ _ \
;; | |_| |  __/ | | |_  | (_| | | | (__| | | | |\ V /  __/ | | | | |_| | | | | | | |  _| | |  __/
;;  \____|\___|_| |_(_)  \__,_|_|  \___|_| |_|_| \_/ \___| |_| |_|\__|_| |_| |_|_| |_| |_|_|\___|
;; ----------------------------------------------


(defun n/blog-create-folder-for-gen-htmls (plist)
  ""
  (let* ((base-dir (n-org-publish-get-elem-in-plist plist :base-directory))
	 (publish-target (n-org-publish-get-elem-in-plist plist :publishing-directory))
	 (gen-htmls-target (concat publish-target
				   n-org-blog-generated-files-folder-name)))
    (when (not (file-exists-p gen-htmls-target))
      (make-directory gen-htmls-target))))

(defun n/blog-assemble-archive (plist)
  "Re-render the blog archive page.
The archive page contains single-line links and dates for every
blog post, but no post body."
  (let* (;; Base-dir (root src path) is needed to substract local base-dir from post-path.
	 (base-dir (n-org-publish-get-elem-in-plist plist :base-directory))
	 (publish-target (n-org-publish-get-elem-in-plist plist :publishing-directory))
	 ;; Have to create folder of files to be generated ;;TODO move down
	 (whateva (n/blog-create-folder-for-gen-htmls plist))
	 (archive-filename (concat publish-target
				   n/blog-archive-file))
         (archive-entries nil)
         (post-filenames (n/blog-get-post-filenames base-dir
							     (n-org-publish-get-elem-in-plist plist :exclude))))
    (progn (message "*** n/blog-assemble-archive ***")
	   ;; (print plist)
	   ;; (print "archive-filenames: ")
	   ;; (print archive-filename)
	   ;; (print "post-filenames: ")
	   ;; (print post-filenames)
	   )
    ;; Remove post-filenames that are not allowed (drafts).
    ;; (setq post-filenames (seq-filter #'(lambda (pf) (not (string-match n/blog-regex-match-not-allowed
    ;; 								       pf)))
    ;; 				     post-filenames))
    
    (setq post-filenames (sort post-filenames (lambda (x y) (time-less-p
                                                        (n/blog-get-date y)
                                                        (n/blog-get-date x)))))
    (n/blog-with-find-file
     archive-filename
     (concat
      "<!DOCTYPE html>\n"
      "<html lang=\"" 
      ;; Lang-code html
      (n-org-publish-get 'lang-code)  ;;"en"
      "\">\n"
      "<head>\n"
      "<meta charset=\"UTF-8\">\n"
      "<link rel=\"alternate\"\n"
      "      type=\"application/rss+xml\"\n"
      "      href=\"" (n/blog-get-complete-rss-url)
      "\"\n"
      "      title=\"RSS feed for " (n-org-publish-get 'url) "\">\n"
      "<title>" (n-org-publish-get 'title) "</title>\n"

      ;;(n/get-string-from-file (n-org-publish-get 'html-snippet-header-file))
      ;;(n-org-publish-get-elem-in-plist plist :html-head)
      ;;TODO: Make some fallback if option in plist isnt there.
      (n/blog-insert-for-gen-htmls-head plist)
      "</head>\n"
      "<body>\n"
      "<div id=\"preamble\" class=\"status\">\n"
      ;;(n/get-string-from-file (n-org-publish-get nil 'html-snippet-preamble-file))
      ;; (n-org-publish-get-elem-in-plist plist
      ;; 				       ;;:html-preamble
      ;; 				       :html-preamble-n-org-blog-gen-htmls)
      (n/blog-insert-for-gen-htmls-preamble plist)
      "</div>\n"
      "<div id=\"content\">\n"
      "<h1 class=\"title\">Archive</h1>\n"
      
      ;;Add tag and rss link
      "<h3 class=\"tag-home-link\">"
      "For tags click "
      "<a href=\"" (n/blog-get-url n/blog-tags-file nil) "\">here</a>\n"
      "and for rss: "
      "<a href=\""
      "../n-org-blog-gen/rss.xml"
      "\">"
      "<img src=\""
      "../resources/pic_rss.gif"
      "\" width=\"36\" height=\"14\">"
      "</a>\n"
      ".</h3>\n"
      
      ;; Archive posts:
      (apply 'concat (mapcar ;;'org-static-blog-get-post-summary
		      #'(lambda (pf) (n/blog-get-post-summary base-dir pf))
			     post-filenames))
      "</body>\n"
      "</html>"))))

(defun n/blog-get-post-summary (base-dir post-filename)
  "Assemble post summary for an archive page.
This function is called for every post on the archive and
tags-archive page. Modify this function if you want to change an
archive headline."
  (progn (message (concat "**** POST-FILENAME: " post-filename))
	 (if (n-org-publish--is-draft? post-filename)
	     ""
	   (concat
	    "<div class=\"post-date\">"
	    (format-time-string "%d %b %Y" (n/blog-get-date post-filename))
	    "</div>"
	    "<h2 class=\"post-title\">"
	    "<a href=\"" (n/blog-get-url post-filename base-dir 1) "\">" 
	    (n/blog-get-title post-filename) "</a>"
	    "</h2>\n"))))

(defun n/blog-get-post-summary-in-org-format (base-dir post-filename)
  "Assemble post summary for tags page. This function is called
for every post on tags page."
  (progn (message (concat "**** POST-FILENAME (ORG): " post-filename))
	 (if (n-org-publish--is-draft? post-filename)
	     ""
	   (concat "- "
		   (format-time-string "%d %b %Y" (n/blog-get-date post-filename))
		   "   "
		   "[[file:"
		   (n/blog-get-url post-filename base-dir 1)
		   "]["
		   (n/blog-get-title post-filename)
		   "]]" "\n"))))



(defun n/blog-fix-links-in-html (html-snip plist &optional str-to-replace root-path input-path)
  "Fn to use to update links in html snippet (to be used in
  PLIST, either in :html-preamble, etc). Inserts
  as much \"../\" in PATH-HTML-SNIP as needed, to make links
  works for :input-file in PLIST.

Usage example:
(... :html-preamble '(lambda (plist) (n/blog-insert-html-preamble \"snips/preamble.html\" plist)) ...)
"
  (let ((str  (or str-to-replace "{{$ROOT_PATH}}"))
	(i    (or input-path     (n-org-publish-get-elem-in-plist plist :input-file)))
	(root (or root-path      (n-org-publish-get-elem-in-plist plist :base-directory))))
    (progn (print "** N/BLOG INSERTING HTML BODY PREAMBLE")
	   ;;(print (n-get-plist-keys plist))
	   (let* ((n (n-get-folder-level-down root i)))
	     ;; (print root)
	     ;; (print i)
	     ;; (print n)
	     ;;(n-print-and-ret ...)
	     (n/replace-in-string str
				  ;; A string of the form: "[../]*"
				  (n/replace-in-string ".."
						       "../"
						       (make-string (* 2 n) ?.))
				  html-snip)
	     ))))

(defun n/blog-insert-html-template (path-html-snip plist &optional str-to-replace root-path input-path)
    "Fn to use in :html-preamble in plist. Inserts as much \"../\"
  in PATH-HTML-SNIP as needed, to make links work for :input-file in PLIST.

Usage example:
(... :html-preamble '(lambda (plist) (n/blog-insert-html-preamble \"snips/preamble.html\" plist)) ...)
"
    (n/blog-fix-links-in-html (n/get-string-from-file path-html-snip)
			      plist
			      str-to-replace
			      root-path input-path))

;; NOTE: These fns will not work with option :html-header in plist, only with preamble and postamble
(defalias 'n/blog-insert-html-preamble   'n/blog-insert-html-template)


(defun n/blog-insert-for-gen-htmls (which-keyword &optional num-levels-down)
  "WHICH-KEYWORD should be :html-head-path-for-gen-htmls or :html-preamble-path-for-gen-htmls."
  ;;n/blog-fix-links-in-html (html-snip plist &optional str-to-replace root-path input-path)
  (print "**************** AQUI!")
  (let ((path-html-snip (n-org-publish-get which-keyword))
	;; (path-html-snip (n-org-publish-get-elem-in-plist plist
	;; 						 which-keyword))
	;; Genearted htmls are going to be one folder in root, meaning num-down=1
	(num-down (or num-levels-down 1)))
    (if (or (not path-html-snip) (not (file-exists-p path-html-snip)))
	(progn (warn (concat "n/blog-insert-for-gen-htmls. Keyword not found.")) nil)
      (n/replace-in-string "{{$ROOT_PATH}}"
			   ;; A string of the form: "[../]*"
			   (n/replace-in-string ".."
						 "../"
						 (make-string (* 2 num-down) ?.))
			   (n/get-string-from-file path-html-snip)))))

(defun n/blog-insert-for-gen-htmls-head (plist) (n/blog-insert-for-gen-htmls 'html-head-path-for-gen-htmls))

(defun n/blog-insert-for-gen-htmls-preamble (plist) (n/blog-insert-for-gen-htmls 'html-preamble-path-for-gen-htmls))



;; ----------------------------------------------
;;  _____           _                              __
;; | ____|_ __   __| |      _   _ ___  ___ _ __   / _|_ __  ___
;; |  _| | '_ \ / _` |_____| | | / __|/ _ \ '__| | |_| '_ \/ __|
;; | |___| | | | (_| |_____| |_| \__ \  __/ |    |  _| | | \__ \
;; |_____|_| |_|\__,_|      \__,_|___/\___|_|    |_| |_| |_|___/
;; ----------------------------------------------

(defun n-org-publish-create-main-index--UNTESTED (plist)
  "Creates index page. Requires n/folder-to-org-file (n-elisp-tools-org-mode.el)."
  ;; Will create main index.org file containing all files.
  (n/folder-to-org-file (n-org-publish-get-elem-in-plist plist :base-directory) nil))

(defun org-static-blog-open-matching-publish-file--BROKEN ()
  "Opens HTML for post."
  (interactive)
  (find-file (n/blog-gen-html-filename (buffer-file-name))))


(defun n-org-publish-call-n/blog-fns (plist)
  "Will call org-static-blog fns to generate rss, and archive page.
Arg plist is the list element of org-publish."
  (progn
    (print (concat "*** n-org-publish-call-n/blog-fns ***"))
    ;; Fns from org-static-blog taken from bastibe:
    (n/blog-assemble-archive plist)
    (n/blog-assemble-rss plist)
    ))

;;;###autoload
(defun org-static-blog-create-new-post (arg)
  "Creates a new blog post (or draft if C-u).
Prompts for a title and proposes a file name. The file name is
only a suggestion; You can choose any other file name if you so
choose."
  (interactive "P")
  (let ((title (read-string "Title: ")))
    (find-file (concat
                 (if arg
		   (n-org-publish-get nil 'drafts-dir-src)
                   (n-org-publish-get nil 'posts-dir-src))
                (read-string "Filename: "
			     (concat (format-time-string "%Y-%m-%d-" (current-time))
				     (replace-regexp-in-string "\s" "-" (downcase title))
				     ".org"))))
    (insert "#+title: " title "\n"
            "#+date: " (format-time-string "<%Y-%m-%d %H:%M>") "\n"
            "#+filetags: ")))

(defun org-static-blog-create-new-post-draft ()
  (interactive)
  (funcall-interactively 'org-static-blog-create-new-post '(4))
  )



(defun n/blog-org-publish-interactive ()
  "Runs org-publish overshadowing org-publish-project-alist."
  (interactive)
  (let ((org-publish-project-alist n/org-blog-publish-project-alist))
    (org-publish)))

(defun n/blog-org-publish (project-alist &optional force)
  "Var n/org-blog-publish-project-alist will overshadow org-publish-project-alist and call org-publish"
  (let ((org-publish-project-alist n/org-blog-publish-project-alist))
    (org-publish project-alist force)))






;; Hydra example:
;; (defhydra my-hydra-n-org-publish (:color pink :columns 1)
;;   "My n-org-publish keybinds"
;;   ("O" org-static-blog-open-previous-post "prev post" :exit t)
;;   ("o" org-static-blog-open-next-post "next post" :exit t)
;;   ("h" org-static-blog-open-matching-publish-file "show html" :exit t)
;;   ("n" org-static-blog-create-new-post "new post" :exit t)
;;   ("N" org-static-blog-create-new-post-draft "new draft" :exit t)
;;   ("c" n-org-publish-see-current "See current proj name" :exit t)
;;   ("C" n-org-publish-change-proj "Change proj" :exit t)
;;   ("P" n-org-publish "n-org-publish projs" :exit t)
;;   ("p" (lambda () (interactive) (n-org-publish nil n-org-publish-current)) "Publish current" :exit t)
;;   ("f" (lambda () (interactive) (n-org-publish '(4) n-org-publish-current)) "Publish current force" :exit t)
;;   ("F" (lambda () (interactive) (n-org-publish '(16) n-org-publish-current)) "Publish current force+async" :exit t)
;;   ("." my-hydra-org/body "Go back to org" :exit t);Go back to root menu
;;   ("q" nil "quit"))

;;;;;; Debugging:

;; Usage example:
;; (require 'n-org-publish)
;; (n-org-publish-set-paths "/home/nm/Documents/Personal/Blog/"
;;                       "/home/nm/Documents/Personal/Output_org_publish/Blog/")
;; (setq n-org-static-blog-title (n-org-publish-get nil 'title) 
;;       n-org-static-blog-url (n-org-publish-get nil 'url))
;; ;;Now, only need to add the blog-settings to org-publish-project-alist. 
;; (setq org-publish-project-alist 
;;       (n-org-publish-get-org-publish-project-alist))

;; Use-package example:
;; (use-package n-org-publish 
;;   :load-path "~/.emacs.d/utilitiesElisp/n-org-publish/"
;;   :pin manual
;;   :after (org)
;;   :config
;;   (n-org-publish-set-paths "/home/nm/Documents/Personal/Blog/"
;;                         "/home/nm/Documents/Personal/Output_org_publish/Blog/")
;;   ;;;;; BEGIN_N_BLOG_EXPORT_TAG
;;   (setq n-org-static-blog-title n/org-static-blog-title 
;;         n-org-static-blog-url (n-org-publish-get nil 'url))
;;   ;;;;; END_N_BLOG_EXPORT_TAG
;;   ;;Now, only need to add the blog-settings to org-publish-project-alist. 
;;   (setq org-publish-project-alist 
;;          (n-org-publish-get-org-publish-project-alist))
;;   )

(provide 'n-org-publish-helper-fns)
;; EVERYTHING:1 ends here
